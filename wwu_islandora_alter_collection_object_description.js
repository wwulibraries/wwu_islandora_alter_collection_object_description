// see readme of https://bitbucket.org/wwulibraries/wwu_islandora_alter_collection_object_description/

document.addEventListener("DOMContentLoaded", function(){
    if (document.getElementsByClassName('islandora-solr-collection-display')) {

        var regex = /\.\, /gm;          // find a period followed by a comma and a space
        var replaceWith = ". ";         // replace it with a period and a space.

        var result_items = document.getElementsByClassName('islandora-object-description');
        for (var i = 0; i < result_items.length; i++) {
            var str = result_items[i].innerText;
            var text = str.replace(regex, replaceWith);
            result_items[i].innerText = text;
        }
    }

});